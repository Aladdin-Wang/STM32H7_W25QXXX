@echo off
set MDK_ROOT=%1



    @echo ==================================================
    @echo If you didn't see "1 file(s) copied", please manually copy STM32H750-ARTPI.FLM to "<KEIL Folder>\Arm\Flash", e.g. "C:\Keil_v5\Arm\Flash\"
    @echo ==================================================
    @copy /B ".\STM32H750-ARTPI.FLM" "%MDK_ROOT%Arm\Flash\STM32H750-ARTPI.FLM"

